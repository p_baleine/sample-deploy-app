var http = require('http'),
    config = require('config');

http.createServer(function(req, res) {
  var body;

  if (req.url === '/') {
    body = '<span>The environmentSpecificValue is <strong>' +
      config.environmentSpecificValue +
      '</strong></span>';

    res.writeHead(200, {
      'Content-Length': body.length,
      'Content-Type': 'text/html'
    });
    res.end(body);
  } else {
    res.statusCode = 404;
    res.end('Not Found');
  }
}).listen(3000);
